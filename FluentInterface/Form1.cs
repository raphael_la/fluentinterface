﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FluentInterface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var date = new MyDate();
            var rc = date.Jahr(2017).And.Monat(12).And.Tag(2).And.Stunde(13).GetDateTime();
            MessageBox.Show(rc.ToString());
        }
    }
}
