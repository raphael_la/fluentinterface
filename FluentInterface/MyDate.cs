﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentInterface
{
    class MyDate
    {
        private DateTime dt = DateTime.MinValue;

        public MyDate And => this;

        public DateTime GetDateTime()
        {
            return dt;
        }

        public MyDate Jahr(int jahr)
        {
            dt = dt.AddYears(jahr - 1);
            return this;
        }
        public MyDate Monat(int monat)
        {
            dt = dt.AddMonths(monat - 1);
            return this;
        }
        public MyDate Tag(int tag)
        {
            dt = dt.AddDays(tag - 1);
            return this;
        }

        public MyDate Stunde(int stunde)
        {
            dt = dt.AddHours(stunde);
            return this;
        }
    }
}
